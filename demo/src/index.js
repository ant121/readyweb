import React from 'react'
import {render} from 'react-dom'
import Example from '../../src'

const App = () => {
  return (
      <div>
        <Example/>
      </div>
  );
};

export default App;

render(<App/>, document.querySelector('#demo'))
