import React from 'react'
import styled from 'styled-components';
import {PlayFill} from '@styled-icons/bootstrap/PlayFill'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0px 20px;
  width: 690px;
  font-family: Gill Sans, sans-serif;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.05), 0 0px 40px rgba(0, 0, 0, 0.08);
  border-radius: 7px;
  justify-content: space-between;
`

const Webbody = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 25px;
`

const TitleWeb = styled.p`
  font-size: 30px;
  font-weight: normal;
  color: #5DB9EB;
`

const TextWeb = styled.p`
  margin: 0px 0px 5px;
  font-size: 18px;
  font-weight: ${props => props.weight ? "bold" : "unset"};
`
const NoteWeb = styled.p`
  margin: 20px 0px 0px;
  font-size: 16px;
  color: #989BA3;
`

const BoldNote = styled.samp`
  font-weight: bold;
`

const ButtonWeb = styled.button`
  background-color: #0494EB;
  color: white;
  font-weight: normal;
  font-size: 17px;
  border: none;
  border-radius: 3px;
  padding: 9px 0px;
  margin-top: 45px;
  cursor: pointer;
  &:hover {
    background-color: #1467CC;
  }
`

const VideoBody = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #F7F7F7;
  padding: 20px 30px;
  margin-left: 25px;
`
const VideoContainer = styled.div`
  background-image: url("https://www.authot.com/wordpress.new/wp-content/uploads/2019/09/online-3412498_1920-720x480.jpg");
  height: 180px;
  background-repeat: no-repeat;
  background-size: cover;
  background-color: dimgrey;
  display: flex;
  justify-content: center;
  align-items: center;
`

const IconVideoButtonContainer = styled.button`
  border: none;
  background-color: #3DA8E0;
  height: 45px;
  width: 45px;
  border-radius: 50%;
  cursor: pointer;
  &:hover {
    background-color: #1467CC;
  }
`

const IconVideoButton = styled(PlayFill)`
  color: white;
`

const TitleVideo = styled.p`
  margin: 20px 0px 10px;
  color: #70798A;
  font-weight: bold;
  font-size: 17px;
`

const TextVideo = styled.p`
  margin: 0px 0px 20px;
  color: #70798A;
  font-size: 16px;
`


const App = () => {
    return (
        <Container>
            <Webbody>
                <TitleWeb>Tu sitio web esta listo</TitleWeb>
                <TextWeb weight={false}>Direccion de tu pagina web:</TextWeb>
                <TextWeb weight>insert64.webnode.com</TextWeb>
                <NoteWeb><BoldNote>Nota:</BoldNote>  Puedes cambiarla por tu propio dominio</NoteWeb>
                <ButtonWeb>Comienza a editar</ButtonWeb>
            </Webbody>
            <VideoBody>
                <VideoContainer>
                    <IconVideoButtonContainer>
                        <IconVideoButton size="35"/>
                    </IconVideoButtonContainer>
                </VideoContainer>
                <TitleVideo>Video tutorial 7 minutos</TitleVideo>
                <TextVideo>Comprueba lo facil que es editar tu web.</TextVideo>
            </VideoBody>
        </Container>
    );
};

export default App;
